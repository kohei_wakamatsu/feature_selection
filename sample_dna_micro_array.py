
# coding: utf-8

# In[1]:


# -*- coding: utf-8 -*-
from keras.models import Sequential
from keras import backend as K
from keras import layers
from keras.layers import Dense, Dropout, Flatten
from keras.initializers import RandomNormal
from keras.optimizers import Adam
from feature_selection import FeatureSelection

from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


# In[2]:


# load data
train  = pd.read_csv('dataset/gene-expression/data_set_ALL_AML_train.csv')
test   = pd.read_csv('dataset/gene-expression/data_set_ALL_AML_independent.csv')
actual = pd.read_csv('dataset/gene-expression/actual.csv')

X_train = train.loc[:, [e for e in train.columns if 'call' not in e]].drop(['Gene Description', 'Gene Accession Number'], axis=1).T
X_test  = test.loc[:, [e for e in test.columns if 'call' not in e]].drop(['Gene Description', 'Gene Accession Number'], axis=1).T.sort_index()
X_train.reset_index(drop=True, inplace=True)
X_test.reset_index(drop=True, inplace=True)

# normarize
scaler = MinMaxScaler()
scaler.fit(X_train)
X_train = scaler.transform(X_train)
X_test  = scaler.transform(X_test)
X_train = np.array(X_train).reshape(38, 7129, 1)
X_test  = np.array(X_test).reshape(34, 7129, 1)

obj_class = 1 * (actual['cancer'] == 'AML')
y_train = np.eye(2)[obj_class][:38]
y_test  = np.eye(2)[obj_class][38:]


# In[3]:


# define model
def all_aml_model():
    model = Sequential()
    
    model.add(FeatureSelection(input_shape=(7129, 1)))
    model.add(Flatten())
              
    model.add(Dense(32, activation='relu'))
    model.add(Dropout(0.25))
    
    model.add(Dense(32, activation='relu'))
    model.add(Dropout(0.25))
              
    model.add(Dense(2, activation='softmax'))
    model.compile(loss='categorical_crossentropy',
                  optimizer=Adam(),
                  metrics=['accuracy'])
    
    return model


# In[4]:


model = all_aml_model()
model.summary()


# In[5]:


# execute learning 50 times
fs_weights50 = []
score50 = []

for i in range(50):
    model = all_aml_model()
    model.fit(X_train, y_train,
              batch_size=38,
              epochs=100,
              verbose=0)
    score50.append(model.evaluate(X_test, y_test, batch_size=34))
    fs_weights50.append(model.get_weights()[0])


# In[6]:


# check result
fs_weights_arr = np.array(fs_weights50)
fs_mean = fs_weights_arr.mean(axis=0)
print("mean of 50 times : {}".format(fs_mean))

# make sorted dataframe
df_res = pd.DataFrame()
df_res['abs fs weight'] = abs(fs_mean.reshape(7129))
df_res['gene no.'] = train['Gene Accession Number']

df_sorted = df_res.sort_values(by='abs fs weight', ascending=False)


# In[7]:


# check result
df_sorted


# In[8]:


# select the top 10 features
effective_list = df_res.sort_values(by='abs fs weight', ascending=False).index[:10]

# combining train and test
df_all = pd.concat([train, test], axis=1)
df_all = df_all.loc[:, [e for e in df_all.columns if 'call' not in e]].drop(['Gene Description', 'Gene Accession Number'], axis=1).T
df_all.index = df_all.index.astype('int') - 1

# add label 'cancer' (take the value of all(=0) or aml(=1))
df_all['cancer'] = actual['cancer']
df_all = df_all.sort_index()

# extract features by effective_list
original = df_all.loc[:, effective_list]
scaler = MinMaxScaler().fit(original)
scaled = scaler.transform(original)
df_eff = pd.DataFrame(scaled)
df_eff['cancer'] = df_all['cancer']

#　visualize
df_eff = df_eff.sort_values(by='cancer')
df_eff_num = df_eff.drop('cancer', axis=1).T
plt.figure(figsize = (20,10))
sns.heatmap(df_eff_num)
plt.show()


# In[9]:


# check training data
df_tra = df_all[:38].loc[:, effective_list]

scaler = MinMaxScaler().fit(df_tra)
scaled_tra = scaler.transform(df_tra)
df_tra = pd.DataFrame(scaled_tra)
df_tra['cancer'] = df_all['cancer'][:38]

#　visualize
_ALL = df_tra[df_tra['cancer'] == 'ALL'].sort_index()
_AML = df_tra[df_tra['cancer'] == 'AML'].sort_index()
df_tra = pd.concat([_ALL, _AML])
df_tra_num = df_tra.drop('cancer', axis=1).T

plt.figure(figsize = (20,10))
sns.heatmap(df_tra_num)
plt.show()


# In[10]:


# check test data
df_test = df_all[38:].loc[:, effective_list]

scaler = MinMaxScaler().fit(df_test)
test_scaled = scaler.transform(df_test)
df_test = pd.DataFrame(test_scaled)
df_test['cancer'] = df_all['cancer'][38:].reset_index(drop=True)

_ALL = df_test[df_test['cancer'] == 'ALL'].sort_index()
_AML = df_test[df_test['cancer'] == 'AML'].sort_index()
df_test = pd.concat([_ALL, _AML])
df_test_num = df_test.drop('cancer', axis=1).T

plt.figure(figsize = (20,10))
sns.heatmap(df_test_num)
plt.show()


# In[11]:


# classify by 10 features
x_red_train = df_eff.drop(['cancer'], axis=1).sort_index()[:38]
x_red_test  = df_eff.drop(['cancer'], axis=1).sort_index()[38:]

# normarize
scaler = MinMaxScaler()
scaler.fit(x_red_train)
x_red_train = scaler.transform(x_red_train)
x_red_test  = scaler.transform(x_red_test)
x_red_train = np.array(x_red_train).reshape(38, 10, 1)
x_red_test  = np.array(x_red_test).reshape(34, 10, 1)


# In[12]:


# define reduce model
def reduce_model():
    model = Sequential()

    model.add(Flatten(input_shape=(10, 1)))
              
    model.add(Dense(32, activation='relu'))
    model.add(Dropout(0.25))
    
    model.add(Dense(32, activation='relu'))
    model.add(Dropout(0.25))
              
    model.add(Dense(2, activation='softmax'))
    model.compile(loss='categorical_crossentropy',
                  optimizer=Adam(),
                  metrics=['accuracy'])
    
    return model


# In[13]:


red_model = reduce_model()
red_model.summary()
red_history = red_model.fit(x_red_train, y_train,
                            batch_size=38,
                            epochs=100,
                            verbose=0)
red_score   = red_model.evaluate(x_red_test, y_test, batch_size=34)
print("score : {}".format(red_score))

