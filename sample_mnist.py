
# coding: utf-8

# In[1]:


# -*- coding: utf-8 -*-
from keras.models import Sequential
from keras import backend as K
from keras import layers
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.layers.advanced_activations import LeakyReLU
from keras.initializers import RandomNormal
from keras.optimizers import Adam
from dataset.mnist import load_mnist

# feature selection
from feature_selection import FeatureSelection
from util import *

# other
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np


# In[2]:


# load mnist with noise
(x_train, t_train), (x_test, t_test) = load_mnist(normalize=True, flatten=False, one_hot_label=True)
x_train = add_noise_mnist(x_train, noise_range=256).transpose(0, 2, 3, 1)
x_test  = add_noise_mnist(x_test, noise_range=256).transpose(0, 2, 3, 1)


# In[3]:


# check data
visualize_mnist(x_train[0])
visualize_mnist(x_test[0])


# In[4]:


# define model 
def mnist_model():
    model = Sequential()
    
    model.add(FeatureSelection(activation='tanh', input_shape=(28, 28, 1)))

    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.25))
              
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.25))
              
    model.add(Dense(10, activation='softmax'))
    model.compile(loss='categorical_crossentropy',
                  optimizer=Adam(),
                  metrics=['accuracy'])
    
    return model


# In[5]:


# check model summary
model = mnist_model()
model.summary()


# In[6]:


# learn
history = model.fit(x_train, t_train,
                    batch_size=256,
                    epochs=10,         
                    verbose=1,         # output log
                    validation_data=(x_test, t_test))
score   = model.evaluate(x_test, t_test, batch_size=256)


# In[7]:


# learning curve
x = [ i for i in range(10)]
plt.plot(history.epoch, history.history['acc'], label='training accuracy')
plt.plot(history.epoch, history.history['val_acc'], label='test accuracy')
    
plt.legend(loc='center left')
plt.show()


# In[8]:


# weight heatmap
fs_weight = abs(model.get_weights()[0]).reshape(28, -1)
plt.figure(figsize=(6,5))
sns.heatmap(fs_weight)
plt.show()

