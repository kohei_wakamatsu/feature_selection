
# coding: utf-8

# In[2]:


# -*- coding: utf-8 -*-
from dataset.mnist import load_mnist

# other
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


# In[8]:


# add noise data to mnist
# replace where the pixel value is 0 with a random(=0~256) value
def add_noise_mnist(org_data, noise_range=256):
    noise = np.random.randint(0, noise_range, org_data.shape[0]*28*28).astype(np.uint8)
    flat = org_data.copy().reshape(-1)
    zero_index = np.where(flat == 0)
    flat[zero_index] = noise[zero_index]
    new_data = flat.reshape(org_data.shape[0], 1, 28, 28)
    
    return new_data

def visualize_mnist(gray_array):
    tmp = gray_array.reshape(28, 28)
    img = np.stack((tmp, tmp, tmp), axis=0).transpose(1, 2, 0)
    plt.imshow(img)
    plt.show()


# In[9]:


if __name__=='__main__':
    if('noise_data' not in locals()):
        (x_train, t_train), (x_test, t_test) = load_mnist(flatten=False, normalize=False, one_hot_label=True)
        noise_data = add_noise_mnist(x_train, noise_range=256)
    visualize_mnist(noise_data[0])

