
# coding: utf-8

# In[1]:


# keras
from keras.models import Sequential
from keras import backend as K
from keras import layers
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.initializers import RandomNormal
from keras.optimizers import Adam
from feature_selection import FeatureSelection

# other
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


# In[2]:


# prepare data
random_data = np.random.rand(300, 10)
np_x = random_data
np_y = np_x[:,0] + np_x[:,1]*2 + np_x[:,2]*3 + np_x[:,3]*4 

# split data
x_train, x_test, y_train, y_test = train_test_split(np_x, np_y, test_size=0.3)


# In[3]:


# modelの定義
def regression_model():
    model = Sequential()
    
    model.add(FeatureSelection(activation='tanh', input_shape=tuple(x_train.shape[1:])))
    
    model.add(Dense(32, activation='relu'))
    model.add(Dropout(0.10))
    
    model.add(Dense(32, activation='relu'))
    model.add(Dropout(0.10))
    
    model.add(Dense(1))
    model.compile(loss='mean_squared_error', optimizer='adam')
    
    return model

model= regression_model()
model.summary()


# In[4]:


# learn
result = model.fit(x_train, y_train, batch_size=32, epochs=100)
score = model.evaluate(x_test, y_test, batch_size=32)


# In[5]:


# 重みを見る用
w_fs_array = abs(model.get_weights()[0]).reshape(1, -1)

plt.figure(figsize=(6,5))
sns.heatmap(w_fs_array)
plt.show()


# In[6]:


# compare by activation function
# まとめて見る用
results = {}
w_fs = {}
b_fs = {}

for act in ['None', 'relu', 'tanh']:
    model = Sequential()
    
    model.add(FeatureSelection(activation=act, input_shape=tuple(x_train.shape[1:])))
    
    model.add(Dense(32, activation='relu'))
    model.add(Dropout(0.10))
    
    model.add(Dense(32, activation='relu'))
    model.add(Dropout(0.10))
    
    model.add(Dense(1))
    model.compile(loss='mean_squared_error', optimizer='adam')
    
    results[act] = model.fit(x_train, y_train, batch_size=32, epochs=100)
    w_fs[act] = abs(model.get_weights()[0]).reshape(1, -1)
    b_fs[act] = abs(model.get_weights()[1]).reshape(1, -1)


# In[7]:


# learning curve
for act in ['None', 'relu', 'tanh']:
    plt.plot(results[act].epoch, results[act].history['loss'], label=act)
    
plt.legend()
plt.show()


# In[8]:


# heatmap
fig = plt.figure(figsize=(19, 10))

for i, act in enumerate(['None', 'relu', 'tanh']):
    plt.subplot(2, 3, i+1)
    sns.heatmap(w_fs[act])
    plt.title("fs weight (" + act + ")")

plt.subplot(2, 3, 5)
sns.heatmap(b_fs['relu'])
plt.title("fs bias (relu)")
plt.show()

