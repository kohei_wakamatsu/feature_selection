
# coding: utf-8

# In[2]:


# coding: utf-8

from keras import layers
from keras import backend as K

class FeatureSelection(layers.Layer):
    
    def __init__(self, activation=None, **kwargs):
        self.activation = activation
        super(FeatureSelection, self).__init__(**kwargs)
        
    def build(self, input_shape):
        self.kernel = self.add_weight(name='kernel', 
                                      shape=tuple(input_shape[1:]),
                                      initializer='uniform')

        if self.activation=='relu':
            self.bias = self.add_weight(name='bias',
                                        shape=tuple(input_shape[1:]),
                                        initializer='zeros')  

        super(FeatureSelection, self).build(input_shape) 

    def call(self, x):
        if self.activation == 'relu':
            return K.relu(x * self.kernel + self.bias)
        elif self.activation == 'tanh':
            return K.tanh(x * self.kernel)
        else:
            return x * self.kernel

    def compute_output_shape(self, input_shape):
        return input_shape


if __name__=='__main__':
    fs = FeatureSelection()
    kvar = K.random_uniform_variable((28,28), 0, 1)
    res = fs(kvar)
    print(K.eval(kvar).shape, K.eval(res).shape)

