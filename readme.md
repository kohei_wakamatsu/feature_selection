簡潔にファイルの説明を記載しておきます。

###feature\_selection.py
kerasで利用できるfeature selection layerクラスです。

###util.py
しょうもない関数が入っています。
mnistにノイズを足したりする関数などが入っています。

###dataset/
mnistやcifar-10やallaml datasetが入っています。

###sample\_mnist.ipynb
ノイズありmnistの分類のサンプルです。
学習曲線やヒートマップの表示も含まれています。

###sample\_dna\_micro\_array.ipynb
DNAマイクロアレイデータセットの分類のサンプルです。
重みを確認したりできます。

上位10個による再学習も含まれています。

###sample\_well\_known\_dataset.ipynb
既知のデータを自分で作ってFS層の性能を確認したものになります。
ヒートマップを見ることができます